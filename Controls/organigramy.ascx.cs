﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace net_organigramy.Controls
{
    public partial class organigramy : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Load_Chart();
        }

        public static void ExecOnStart2(string scname, string script)
        {
            Page page = HttpContext.Current.Handler as Page;
            ScriptManager.RegisterStartupScript(page, typeof(Page), scname, script, true);
        }

        protected bool gViewState
        {
            get
            {
                return Session["ViewState"] == null || Session["ViewState"].Equals("vertical");
            }
        }

        #region Ładowanie wykresu
        protected void Load_Chart()
        {
            QueryManager.Open();

            string sql = printStruct.Checked ? struct_sql.SelectCommand : dsOrg.SelectCommand;

            SqlCommand sqlCmd = new SqlCommand(sql, QueryManager.c_handler);

            DateTime date;
            if (!DateTime.TryParse(tbNaDzien.Text, out date))
            {
                date = DateTime.Now;
                tbNaDzien.Text = date.ToString("yyyy-MM-dd");
            }

            sqlCmd.Parameters.Add("@date", SqlDbType.DateTime);
            sqlCmd.Parameters["@date"].Value = date.Date;

            try
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "ChangeFlag('" + (gViewState ? "horizontal" : "vertical") + "');", true);
                send_Script(prepare_JSON(sqlCmd.ExecuteReader()));
            }
            finally
            {
                QueryManager.Close();
            }
        }

        protected string prepare_JSON(SqlDataReader reader)
        {
            List<List<object>> infos = new List<List<object>>();

            while (reader.Read())
            {
                List<object> cols = new List<object>() {
                    reader["Id"].ToString(),
                    string.Join(";", new [] {
                        reader["Pracownik"],
                        reader["Stanowisko"],
                        reader["Telefon"],
                        reader["Email"],
                        reader["Avatar"],
                        reader["Typ"],
                        reader["Css"],
                        reader["Opis"].ToString().Replace(";", ","),
                        reader["Avatar"],
                        reader["Od"],
                        reader["Do"]
                    }),
                    reader["IdKierownika"].Equals(0) ? "" : reader["IdKierownika"].ToString()
                };

                infos.Add(cols);
            }

            return new JavaScriptSerializer().Serialize(infos);
        }

        protected void send_Script(string json)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "CallMyFunction", "<script type='text/javascript'>runChart('" + json + "');</script>", false);
        }
        
        protected void ChangeView(object sender, EventArgs e)
        {
            if (gViewState)
            {
                Session["ViewState"] = "horizontal";
            }
            else
            {
                Session["ViewState"] = "vertical";
            }

            Response.Redirect(Request.RawUrl);
        }
        #endregion

        #region Edycja istniejących obiektów
        protected void existingDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (modalWykres_existingDropdown.SelectedValue.Equals(""))
                return;
            
            QueryManager.Open();
            SqlCommand sqlCmd = new SqlCommand(pracownik_byid.SelectCommand, QueryManager.c_handler);

            sqlCmd.Parameters.Add("@id", SqlDbType.Int);
            sqlCmd.Parameters["@id"].Value = modalWykres_existingDropdown.SelectedValue;

            SqlDataReader reader = sqlCmd.ExecuteReader();
            
            try
            {
                reader.Read();

                DateTime dataOd, dataDo;

                if (reader["Typ"].Equals(2))
                {
                    modalWykres_multiView.SetActiveView(multiView_2);

                    modalWykres_struct_name.Text = reader["Nazwisko"].ToString();
                    if (!DateTime.TryParse(reader["Od"].ToString(), out dataOd))
                    {
                        modalWykres_struct_od.Text = reader["Od"].ToString();
                    }
                    else
                    {
                        modalWykres_struct_od.Text = dataOd.ToString("yyyy-MM-dd");
                    }

                    if (!DateTime.TryParse(reader["Do"].ToString(), out dataDo))
                    {
                        modalWykres_struct_do.Text = reader["Do"].ToString();
                    }
                    else
                    {
                        modalWykres_struct_do.Text = dataDo.ToString("yyyy-MM-dd");
                    }
                        
                }
                else
                {
                    modalWykres_multiView.SetActiveView(multiView_1);

                    modalWykres_worker_imie.Text = reader["Imie"].ToString();
                    modalWykres_worker_nazwisko.Text = reader["Nazwisko"].ToString();
                    modalWykres_worker_telefon.Text = reader["Telefon"].ToString();
                    modalWykres_worker_email.Text = reader["Email"].ToString();
                    modalWykres_worker_opis.Text = reader["Opis"].ToString();
                    modalWykres_worker_image.Value = reader["Avatar"].ToString();
                    modalWykres_worker_plec.SelectedValue = reader["Typ"].ToString();

                    string stan = reader["IdStanowiska"].ToString();
                    if (stan.Equals(0) || modalWykres_worker_stanowisko.Items.FindByValue(stan) == null)
                    {
                        modalWykres_worker_stanowisko.SelectedIndex = 0;
                    }
                    else
                    {
                        modalWykres_worker_stanowisko.SelectedValue = stan;
                    }

                    
                    if (!DateTime.TryParse(reader["Od"].ToString(), out dataOd))
                    {
                        modalWykres_worker_od.Text = reader["Od"].ToString();
                    }
                    else
                    {
                        modalWykres_worker_od.Text = dataOd.ToString("yyyy-MM-dd");
                    }

                    if (!DateTime.TryParse(reader["Do"].ToString(), out dataDo))
                    {
                        modalWykres_worker_do.Text = reader["Do"].ToString();
                    } 
                    else
                    {
                        modalWykres_worker_do.Text = dataDo.ToString("yyyy-MM-dd");
                    }
                }

                modalWykres_id.Value = reader["Id"].ToString();

                var value = reader["IdKierownika"].ToString();
                if (value.Equals(DBNull.Value) || value.Equals(0) || modalWykres_podleglosc.Items.FindByValue(value) == null)
                {
                    modalWykres_podleglosc.SelectedIndex = 0;
                }
                else
                {
                    modalWykres_podleglosc.SelectedValue = value;
                }
                    
            }
            finally
            {
                QueryManager.Close();
            }
        }
        #endregion

        #region Czyszczenie modala
        protected void clean_modal()
        {
            modalWykres_id.Value = null;
            modalWykres_worker_imie.Text = null;
            modalWykres_worker_nazwisko.Text = null;
            modalWykres_worker_telefon.Text = null;
            modalWykres_worker_email.Text = null;
            modalWykres_worker_opis.Text = null;
            modalWykres_worker_od.Text = null;
            modalWykres_worker_do.Text = null;
            modalWykres_worker_stanowisko.SelectedIndex = 0;
            modalWykres_worker_plec.SelectedIndex = 0;

            modalWykres_struct_name.Text = null;
            modalWykres_struct_od.Text = null;
            modalWykres_struct_do.Text = null;
            
            modalWykres_podleglosc.SelectedIndex = 0;
        }
        #endregion
        
        #region Upload zdjęć
        protected int NextFreeEmployeeID()
        {
            QueryManager.Open();

            SqlCommand sqlCmd = new SqlCommand("SELECT MAX([Id]) + 1 AS max_vl FROM [Pracownicy]", QueryManager.c_handler);
            SqlDataReader reader = sqlCmd.ExecuteReader();
            try
            {
                reader.Read();
                return int.Parse(reader["max_vl"].ToString());
            }
            finally
            {
                QueryManager.Close();
            }
        }

        protected void FileUploadComplete(object sender, EventArgs e)
        {
            int free_id = NextFreeEmployeeID();
            string file_name = Path.GetFileName(AsyncImageUpload.FileName);
            string file_extension = file_name.Substring(file_name.LastIndexOf('.') + 1);
            if (new string[] { "png", "jpg", "jpeg" }.Contains(file_extension))
            {
                // TODO
                // error
                return;
            }

            // TODO
            // Size Limit

            string new_file_name = free_id + "_tmp." + file_extension;
            string new_name = free_id + ".png";

            AsyncImageUpload.SaveAs(Server.MapPath("Upload/Images/") + new_file_name);
            CreateThumbnail(Server.MapPath("Upload/Images/") + new_file_name, new_name);

            FileInfo myfileinf = new FileInfo(Server.MapPath("Upload/Images/" + new_file_name));
            myfileinf.Delete();

            Session["Avatar"] = new_name;
        }

        public void CreateThumbnail(string path, string new_name)
        {
            System.Drawing.Image image = System.Drawing.Image.FromFile(path);

            int width = 64, height = 64;

            Bitmap thumbnailBitmap = new Bitmap(width, height);
            Graphics thumbnailGraph = Graphics.FromImage(thumbnailBitmap);

            thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
            thumbnailGraph.SmoothingMode = SmoothingMode.HighQuality;
            thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
            thumbnailGraph.DrawImage(image, new Rectangle(0, 0, width, height));
            thumbnailBitmap.Save(Server.MapPath("Upload/Images/" + new_name), ImageFormat.Jpeg);
            thumbnailGraph.Dispose();
            thumbnailBitmap.Dispose();
            image.Dispose();
        }
        #endregion

        #region Zdarzenia
        protected void printStruct_CheckedChanged(object sender, EventArgs e)
        {
            Load_Chart();
        }

        protected void cleanModal_Click(object sender, EventArgs e)
        {
            clean_modal();
        }

        protected void select_worker_Click(object sender, EventArgs e)
        {
            clean_modal();

            modalWykres_multiView.SetActiveView(multiView_1);
        }

        protected void select_struct_Click(object sender, EventArgs e)
        {
            clean_modal();

            modalWykres_multiView.SetActiveView(multiView_2);
        }

        protected void show_modalWykres_Click(object sender, EventArgs e)
        {
            modalWykres.Show(true);
            Load_Chart();
        }

        protected void acceptModal_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;

            QueryManager.Open();
            try
            {
                bool mode = modalWykres_id.Value.Equals(""); // 1 insert | 0 update
                int activeView = modalWykres_multiView.ActiveViewIndex;
                string sql = mode ? pracownik_byid.InsertCommand : pracownik_byid.UpdateCommand;
                SqlCommand sqlCmd = new SqlCommand(sql, QueryManager.c_handler);

                if (!mode)
                {
                    if (modalWykres_id.Value == modalWykres_podleglosc.SelectedValue)
                        return;

                    int IdPracownika;
                    if (int.TryParse(modalWykres_id.Value, out IdPracownika))
                    {
                        sqlCmd.Parameters.AddWithValue("@pracownik", IdPracownika);
                    }
                }
                else
                {
                    modalWykres.Close();
                }


                sqlCmd.Parameters.AddWithValue("@avatar", Session["Avatar"] != null ? Session["Avatar"].ToString() : modalWykres_worker_image.Value);

                sqlCmd.Parameters.AddWithValue("@imie", modalWykres_worker_imie.Text);
                sqlCmd.Parameters.AddWithValue("@nazwisko", activeView.Equals(2) ? modalWykres_struct_name.Text : modalWykres_worker_nazwisko.Text);
                sqlCmd.Parameters.AddWithValue("@telefon", modalWykres_worker_telefon.Text);
                sqlCmd.Parameters.AddWithValue("@email", modalWykres_worker_email.Text);
                sqlCmd.Parameters.AddWithValue("@opis", modalWykres_worker_opis.Text);
                sqlCmd.Parameters.AddWithValue("@stanowisko", modalWykres_worker_stanowisko.SelectedValue);
                sqlCmd.Parameters.AddWithValue("@parent", modalWykres_podleglosc.SelectedValue);
                sqlCmd.Parameters.AddWithValue("@typ", activeView.Equals(2) ? "2" : modalWykres_worker_plec.SelectedValue);

                System.Diagnostics.Debug.WriteLine(modalWykres_podleglosc.SelectedValue);

                DateTime dataOd, dataDo;

                if (DateTime.TryParse(activeView == 2 ? modalWykres_struct_od.Text : modalWykres_worker_od.Text, out dataOd))
                {
                    if (DateTime.TryParse(activeView == 2 ? modalWykres_struct_do.Text : modalWykres_worker_do.Text, out dataDo))
                    {
                        dataDo = dataOd >= dataDo ? dataOd.AddDays(14) : dataDo;
                    }
                    else
                    {
                        dataDo = dataOd.AddDays(14);
                    }

                    sqlCmd.Parameters.AddWithValue("@od", dataOd);
                    sqlCmd.Parameters.AddWithValue("@do", dataDo);
                }

                sqlCmd.ExecuteNonQuery();
            }
            finally
            {
                Session["Avatar"] = null;
                QueryManager.Close();
            }

        }
        #endregion
    }

    #region Zarządzanie SQL
    public static class QueryManager
    {
        public static SqlConnection c_handler { set; get; }

        public static void Open()
        {
            c_handler = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            c_handler.Open();
        }

        public static void Close()
        {
            c_handler.Close();
        }
    }
    #endregion 
}