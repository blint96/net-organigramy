﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="organigramy.ascx.cs" Inherits="net_organigramy.Controls.organigramy" %>
<%@ Register Src="~/Controls/cntModal.ascx" TagPrefix="uc1" TagName="cntModal" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!-- uprawnienia -->
<asp:HiddenField ID="perm" runat="server" Value="true" />

<!-- modal pdf -->
<uc1:cntModal runat="server" ID="cntModalPDF" CssClass="pdf-class">

    <HeaderTemplate>
        <h4>Wydruk PDF</h4>
    </HeaderTemplate>

    <ContentTemplate>
        <h5>Czy jesteś pewien, że chcesz wygenerować PDF?</h5>
    </ContentTemplate>

    <FooterTemplate>
        <button class="btn btn-primary" onclick="ExportToPDF();" data-dismiss="modal">Wygeneruj</button>
    </FooterTemplate>

</uc1:cntModal>

<!-- modal brak uprawnien -->
<uc1:cntModal runat="server" ID="modalAlert" ShowHeader="false" ShowFooter="false">
    <ContentTemplate>
        <div class="container">
            <div class="row">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <h3>Nie możesz tego zrobić !</h3>
                <h5>
                    <asp:Label runat="server" ID="modalAlert_message"></asp:Label></h5>
            </div>
        </div>
    </ContentTemplate>
</uc1:cntModal>

<!-- modal wykres -->
<uc1:cntModal runat="server" ID="modalWykres">

    <HeaderTemplate>
        <h4>Modyfikacja wykresu</h4>
    </HeaderTemplate>

    <ContentTemplate>
        <asp:HiddenField runat="server" Visible="false" ID="modalWykres_id" />

        <div class="container-fluid">

            <!-- Edycja istniejącego obiektu -->
            <div runat="server" class="row">
                <div class="form-group">
                    <label for="modalWykres_existing">Wybierz obiekt do edycji:</label>
                    <asp:DropDownList ID="modalWykres_existingDropdown" class="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="existingDropdown_SelectedIndexChanged" AppendDataBoundItems="true" DataSourceID="pracownicy" DataTextField="Dane" DataValueField="Id">
                        <Items>
                            <asp:ListItem Text="Wybierz użytkownika.." Value="" Selected="True" />
                        </Items>
                    </asp:DropDownList>
                    <asp:SqlDataSource runat="server" ID="pracownicy" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' SelectCommand="SELECT [Id], CONCAT([Imie], ' ', [Nazwisko]) As Dane, [Typ] FROM [Pracownicy]"></asp:SqlDataSource>
                </div>
            </div>

            <asp:MultiView ID="modalWykres_multiView" runat="server" ActiveViewIndex="0">

                <!-- menu wyboru -->
                <asp:View ID="multiView_0" runat="server">
                    <div runat="server" id="select" class="row">
                        <div class="row well">
                            <h5 id="menuHeader"><strong>chyba, że</strong> chcesz umieścić nowy obiekt..</h5>

                            <div class="row">

                                <!-- pracownik -->
                                <div class="col-md-6">
                                    <div class="buttns">
                                        <div class="form-group">
                                            <button runat="server" onserverclick="select_worker_Click" class="btn btn-default btn-block">Pracownik</button>
                                        </div>
                                    </div>
                                </div>

                                <!-- struktura -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <button runat="server" onserverclick="select_struct_Click" class="btn btn-default btn-block">Struktura</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>

                <!-- pracownik -->
                <asp:View ID="multiView_1" runat="server">
                    <div id="worker" class="row well" style="border-radius: 0;">
                        <asp:HiddenField runat="server" Visible="false" ID="modalWykres_worker_image" />
                        <h4>Dane pracownika:</h4>

                        <div class="row">

                            <!-- imie pracownika -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox MaxLength="20" class="form-control" ID="modalWykres_worker_imie" runat="server" Placeholder="Imię" ValidationGroup="modalWykres"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="modalWykres_worker_imie" runat="server" ErrorMessage="* To pole jest wymagane" ForeColor="red" EnableClientScript="False" Display="Dynamic" ValidationGroup="modal"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <!-- nazwisko pracownika -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:TextBox MaxLength="20" class="form-control" ID="modalWykres_worker_nazwisko" runat="server" Placeholder="Nazwisko"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="modalWykres_worker_nazwisko" runat="server" ErrorMessage="* To pole jest wymagane" ForeColor="red" EnableClientScript="False" Display="Dynamic" ValidationGroup="modalWykres"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <!-- telefon -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <asp:TextBox MaxLength="12" class="form-control" ID="modalWykres_worker_telefon" runat="server" Placeholder="Telefon"></asp:TextBox>
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></div>
                                    </div>
                                    <asp:RequiredFieldValidator ControlToValidate="modalWykres_worker_telefon" runat="server" ErrorMessage="* To pole jest wymagane" ForeColor="red" EnableClientScript="False" Display="Dynamic" ValidationGroup="modalWykres"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <!-- email -->
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="input-group">
                                        <asp:TextBox MaxLength="40" class="form-control" ID="modalWykres_worker_email" runat="server" Placeholder="Email"></asp:TextBox>
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                                    </div>
                                    <asp:RequiredFieldValidator ControlToValidate="modalWykres_worker_email" runat="server" ErrorMessage="* To pole jest wymagane" ForeColor="red" EnableClientScript="False" Display="Dynamic" ValidationGroup="modalWykres"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="red" ControlToValidate="modalWykres_worker_email" ErrorMessage="* Błędna składnia dla adresu email" EnableClientScript="False" Display="Dynamic" ValidationGroup="modalWykres"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <!-- stanowisko -->
                            <div class="col-md-9">
                                <div class="form-group">
                                    <asp:DropDownList ID="modalWykres_worker_stanowisko" class="form-control" runat="server" DataSourceID="stanowiska" DataTextField="Stanowisko" DataValueField="Id" AppendDataBoundItems="true">
                                        <Items>
                                            <asp:ListItem Text="Wybierz stanowisko.." Value="" Selected="True" />
                                        </Items>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource runat="server" ID="stanowiska" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' SelectCommand="SELECT * FROM [lStanowiska]"></asp:SqlDataSource>
                                    <asp:RequiredFieldValidator ControlToValidate="modalWykres_worker_stanowisko" runat="server" ErrorMessage="* To pole jest wymagane" ForeColor="red" EnableClientScript="False" Display="Dynamic" ValidationGroup="modalWykres"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <!-- plec -->
                            <div class="col-md-3">
                                <asp:DropDownList ID="modalWykres_worker_plec" runat="server" class="form-control">
                                    <asp:ListItem Text="Płeć" Value="" Selected="True" />
                                    <asp:ListItem Value="0">Kobieta</asp:ListItem>
                                    <asp:ListItem Value="1">Mężczyna</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator ControlToValidate="modalWykres_worker_plec" runat="server" ErrorMessage="* To pole jest wymagane" ForeColor="red" EnableClientScript="False" Display="Dynamic" ValidationGroup="modalWykres"></asp:RequiredFieldValidator>
                        </div>

                        <div class="row">

                            <!-- opis -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:TextBox MaxLength="150" class="form-control" ID="modalWykres_worker_opis" runat="server" Placeholder="Opis.." Rows="3"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="modalWykres_worker_opis" runat="server" ErrorMessage="* To pole jest wymagane" ForeColor="red" EnableClientScript="False" Display="Dynamic" ValidationGroup="modalWykres"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <h4>Przedział czasowy wyświetlania:</h4>

                        <div class="row">

                            <!-- data od -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <asp:TextBox class="datepicker textbox dateedit form-control" ID="modalWykres_worker_od" runat="server" Placeholder="Kliknij, aby wprowadzić.."></asp:TextBox>
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
                                    </div>
                                    <asp:RequiredFieldValidator ControlToValidate="modalWykres_worker_od" runat="server" ErrorMessage="* To pole jest wymagane" ForeColor="red" EnableClientScript="False" Display="Dynamic" ValidationGroup="modalWykres"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <!-- data do -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <asp:TextBox class="datepicker textbox dateedit form-control" ID="modalWykres_worker_do" runat="server" Placeholder="Kliknij, aby wprowadzić.."></asp:TextBox>
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <!-- upload -->
                            <div class="col-md-5">
                                <div class="form-group">
                                    <cc1:AsyncFileUpload runat="server" ID="AsyncImageUpload" OnUploadedComplete="FileUploadComplete" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>

                <!-- struktura -->
                <asp:View ID="multiView_2" runat="server">
                    <div id="struct" class="row well">
                        <h4>Dane struktury:</h4>

                        <div class="row">

                            <!-- nazwa -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:TextBox MaxLength="20" class="form-control" ID="modalWykres_struct_name" runat="server" Placeholder="Nazwa"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="modalWykres_struct_name" runat="server" ErrorMessage="* To pole jest wymagane" ForeColor="red" EnableClientScript="False" Display="Dynamic" ValidationGroup="modalWykres"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <h4>Przedział czasowy wyświetlania:</h4>

                        <div class="row">

                            <!-- data od -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <asp:TextBox class="datepicker textbox dateedit form-control" ID="modalWykres_struct_od" runat="server" Placeholder="Kliknij, aby wprowadzić.."></asp:TextBox>
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
                                    </div>
                                    <asp:RequiredFieldValidator ControlToValidate="modalWykres_struct_od" runat="server" ErrorMessage="* To pole jest wymagane" ForeColor="red" EnableClientScript="False" Display="Dynamic" ValidationGroup="modalWykres"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <!-- data do -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <asp:TextBox class="datepicker textbox dateedit form-control" ID="modalWykres_struct_do" runat="server" Placeholder="Kliknij, aby wprowadzić.."></asp:TextBox>
                                        <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>

            <div id="podleglosc" class="row" runat="server">
                <label>Obiekt nadrzędny:</label>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <asp:DropDownList ID="modalWykres_podleglosc" class="form-control" runat="server" DataSourceID="inni" DataTextField="Column1" DataValueField="Id" AppendDataBoundItems="true">
                                <Items>
                                    <asp:ListItem Text="Do kogo podpiąć?" Value="0" Selected="True" />
                                </Items>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </ContentTemplate>
    <FooterTemplate>
        <button runat="server" onserverclick="acceptModal_Click" type="button" class="btn btn-default" validationgroup="modalWykres">Potwierdzam formularz</button>
        <button runat="server" onserverclick="cleanModal_Click" type="button" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
    </FooterTemplate>
</uc1:cntModal>

<!-- logo -->
<div id="logo">
    <a href="http://www.visscher-caravelle.nl/">
        <img src="Content/images/logo.png" />
    </a>
</div>

<!-- zwinięte menu -->
<div class="navbar navbar-default" id="swMenu">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
        <ul class="nav navbar-nav">
            <li data-toggle="tooltip" data-placement="bottom" title="Rozwiń menu"><a href="#" data-toggle="collapse" data-target="#menu"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
        </ul>
    </div>
</div>

<!-- rozwinięte menu -->
<nav class="navbar navbar-default collapse in" id="menu">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <!-- zwiń menu -->
                <li data-toggle="tooltip" data-placement="bottom" title="Zwiń menu">
                    <a href="javascript:" data-toggle="collapse" data-target="#menu">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                </li>

                <!-- drukuj pdf -->
                <li data-toggle="tooltip" data-placement="bottom" title="Wyeksportuj do PDF">
                    <a href="javascript:" data-toggle="modal" data-target=".pdf-class">
                        <span class="glyphicon glyphicon-file"></span>
                    </a>
                </li>

                <!-- modyfkuj wykres -->
                <li runat="server" data-toggle="tooltip" data-placement="bottom" title="Modyfikuj wykres" visible='<%# bool.Parse(perm.Value) %>'>
                    <a id="add_worker" runat="server" onserverclick="show_modalWykres_Click" href="javascript:">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                </li>

                <!-- zwiń/rozwij opisy -->
                <li data-toggle="tooltip" data-placement="bottom" title="Zwiń/Rozwiń wszystkie opisy">
                    <a href="javascript:" onclick="ChangeAllCollapse(); return false;">
                        <span class="collapse-expand glyphicon glyphicon-chevron-down"></span>
                    </a>
                </li>

                <!-- zmiana widoku -->
                <li data-toggle="tooltip" data-placement="bottom" title="Zmiana widoku">
                    <a id="chview" href="javascript:" runat="server" onserverclick="ChangeView">
                        <span class="view-change glyphicon glyphicon <%=(gViewState ? "glyphicon-option-horizontal" : "glyphicon-option-vertical") %>"></span>
                    </a>
                </li>
            </ul>
            <div class="navbar-form navbar-left">

                <!-- wyszukaj po dacie -->
                <div class="form-group">
                    <span>Na dzień:</span>
                    <div class="input-group">
                        <asp:TextBox ID="tbNaDzien" runat="server" CssClass="datepicker textbox dateedit form-control" MaxLength="10" Visible="true" Placeholder="Kliknij, aby wybrać.."></asp:TextBox>
                        <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
                    </div>
                    <input placeholder="Wyszukaj.." type="text" id="searchBox" class="form-control" />
                </div>

                <button type="button" id="filters_Click" class="btn btn-default"><span class="glyphicon glyphicon-list-alt"></span></button>

                <!-- filtry wyświetlania -->
                <div id="filters-pop" style="display: none;">
                    <div class="head hide">Filtry wyświetlania</div>
                    <div class="content hide">
                        <div id="filters-content" class="container-fluid">
                            <div class="row">

                                <!-- stanowisko -->
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input class="filters" type="checkbox" data-element="worker-title" checked> Nazwa stanowiska
                                        </label>
                                    </div>
                                </div>

                                <!-- imie i nazwisko -->
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input class="filters" type="checkbox" data-element="worker-name" checked> Imię i Nazwisko
                                        </label>
                                    </div>
                                </div>

                                <!-- zdjecie -->
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input class="filters" type="checkbox" data-element="worker-avatar" checked> Awatar
                                        </label>
                                    </div>
                                </div>

                                <!-- telefon -->
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input class="filters" type="checkbox" data-element="worker-phone" checked> Numer telefonu
                                        </label>
                                    </div>
                                </div>

                                <!-- email -->
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input class="filters" type="checkbox" data-element="worker-mail" checked> Adres e-mail
                                        </label>
                                    </div>
                                </div>

                                <!-- licznik -->
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input class="filters" type="checkbox" data-element="worker-counter" checked> Licznik podległych
                                        </label>
                                    </div>
                                </div>

                                <!-- tylko struktura -->
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <asp:CheckBox ID="printStruct" CssClass="filters" data-element="worker-struct" OnCheckedChanged="printStruct_CheckedChanged" runat="server" AutoPostBack="true" /> Schowaj pracowników
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button onclick="search()" type="button" id="btn_search" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
            </div>
        </div>
    </div>
</nav>

<asp:UpdatePanel runat="server" UpdateMode="Conditional">

    <ContentTemplate>
        <div id="body_content">
            <div class="container-fluid">
                
                <!-- lista -->
                <div id="result"></div>

                <!-- wykres -->
                <div id="tree-view"></div>
            </div>
        </div>
    </ContentTemplate>

    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="add_worker" />
        <asp:AsyncPostBackTrigger ControlID="tbNaDzien" />
        <asp:AsyncPostBackTrigger ControlID="printStruct" />
        <asp:AsyncPostBackTrigger ControlID="chview" />
    </Triggers>

</asp:UpdatePanel>


<%: Scripts.Render("~/bundles/jsPDF") %>
<%: Scripts.Render("~/bundles/Organigramy") %>

<asp:SqlDataSource ID="dsOrg" runat="server"
    SelectCommand="
with hs(root, hsid, h, s) as
(
	select r.IdKierownika
    , r.IdPracownika, 0
    , CAST(CAST(r.IdKierownika as VARBINARY(4)) as VARBINARY(8000))
    from Pracownicy p 
    inner join Hierarchia r on r.IdPracownika = p.Id and r.Status = 1 and @date between r.Od and ISNULL(r.Do, '20990909')

	union all

	select r.IdKierownika
    , hs.hsid
    , hs.h + 1
    , CAST(CAST(r.IdKierownika as VARBINARY(4)) + hs.s as VARBINARY(8000))
    from Pracownicy p inner join Hierarchia r on r.IdPracownika = p.Id and r.Status = 1 and @date between r.Od and ISNULL(r.Do, '20990909')
	inner join hs on hs.root = r.IdPracownika
)
select CAST(hs.s + CAST(hs.hsid as VARBINARY(4)) as VARBINARY(8000)) sort
, p.Id
, p.Imie + ' ' + p.Nazwisko as Pracownik
, CASE WHEN p.Typ = 2 THEN p.Nazwisko ELSE s.Stanowisko END AS Stanowisko
, h.IdKierownika
, p.Telefon
, p.Email
, p.Opis
, p.Avatar
, p.Typ
, p.Css
, h.Od
, h.Do
from hs
left join Pracownicy p on p.Id = hs.hsid
left join lStanowiska s on s.Id = IdStanowiska
left join Hierarchia h on h.IdPracownika = p.Id
where hs.root = 0
order by sort
option (maxrecursion 0)
    "></asp:SqlDataSource>


<asp:SqlDataSource ID="pracownik_byid" runat="server"
    SelectCommand="
SELECT p.Id
, p.Imie
, p.Nazwisko
, p.Telefon
, p.Email
, p.Opis
, p.Avatar
, p.IdStanowiska
, h.IdKierownika
, p.Typ
, h.Od
, h.Do
FROM Pracownicy As p
LEFT JOIN Hierarchia As h ON p.Id = h.IdPracownika
WHERE p.Id = @id
    "
    UpdateCommand="
DECLARE @id_pracownika AS int
DECLARE @old_date AS Date;
DECLARE @old_id AS int;

BEGIN TRANSACTION

UPDATE Pracownicy
SET @id_pracownika = Id
, Imie = @imie
, Nazwisko = @nazwisko
, Telefon = @telefon
, Email = @email
, Avatar = @avatar
, Opis = @opis
, Typ = @typ
, IdStanowiska = @stanowisko
WHERE Pracownicy.Id = @pracownik;


SELECT TOP 1 @old_date = Od
, @old_id = Id 
FROM Hierarchia 
WHERE IdPracownika = @id_pracownika 
ORDER BY Id DESC;

IF @old_id IS NOT NULL
	IF @old_date != CAST(@od as date)
		BEGIN
			INSERT INTO Hierarchia (IdPracownika, IdKierownika, Od, Do)
			VALUES (@id_pracownika, @parent, @od, @do)

			UPDATE Hierarchia
			SET Do = @do
			FROM Hierarchia 
			WHERE Id = @old_id;
		END
	ELSE
		BEGIN
			UPDATE Hierarchia
			SET IdKierownika = @parent
			, Do = @do
			FROM Hierarchia 
			WHERE Id = @old_id;
		END
ELSE
	INSERT INTO Hierarchia (IdPracownika, IdKierownika, Od, Do)
    VALUES (@id_pracownika, @parent, @od, @do)

COMMIT;
    "
    InsertCommand="
DECLARE @id AS int;
BEGIN TRANSACTION
    INSERT INTO Pracownicy (Imie, Nazwisko, Telefon, Email, Opis, Avatar, Typ, IdStanowiska)
    VALUES (@imie, @nazwisko, @telefon, @email, @opis, @avatar, @typ, @stanowisko);

    SELECT @id = SCOPE_IDENTiTY();

    INSERT INTO Hierarchia (IdPracownika, IdKierownika, Od, Do)
    VALUES (@id, @parent, @od, @do)
COMMIT;
    "></asp:SqlDataSource>

<asp:SqlDataSource ID="struct_sql" runat="server"
    SelectCommand="
with hs(root, hsid, od, do, h) as
(
	select h.IdKierownika, h.IdPracownika, h.Od, h.Do, 0 from Hierarchia h WHERE @date between h.Od and ISNULL(h.Do, '20990909')
	union all
	select h.IdKierownika, hs.hsid, hs.od, hs.do, hs.h + 1 from Hierarchia h inner join hs on hs.root = h.IdPracownika WHERE @date between h.Od and ISNULL(h.Do, '20990909')
)
select p.Id
, ISNULL(a.root, 0) As IdKierownika
, p.Imie + ' ' + p.Nazwisko as Pracownik
, CASE WHEN p.Typ = 2 THEN p.Nazwisko ELSE s.Stanowisko END AS Stanowisko
, p.Telefon
, p.Email
, p.Opis
, p.Avatar
, p.Typ
, p.Css
, a.od
, a.do
from Pracownicy p
outer apply (select top 1 * from hs inner join Pracownicy p0 on p0.Id = hs.root and p0.Typ = 2 where hs.hsid = p.Id order by hs.h) a
left join lStanowiska s on s.Id = p.IdStanowiska
where p.Typ = 2
    "></asp:SqlDataSource>

<asp:SqlDataSource runat="server" ID="inni" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'
    SelectCommand="
with hs(root, hsid, h, s) as
(
	select r.IdKierownika
    , r.IdPracownika, 0
    , CAST(CAST(r.IdKierownika as VARBINARY(4)) as VARBINARY(8000))
    from Pracownicy p 
    inner join Hierarchia r on r.IdPracownika = p.Id and r.Status = 1 and GETDATE() between r.Od and ISNULL(r.Do, '20990909')

	union all

	select r.IdKierownika
    , hs.hsid
    , hs.h + 1
    , CAST(CAST(r.IdKierownika as VARBINARY(4)) + hs.s as VARBINARY(8000))
    from Pracownicy p inner join Hierarchia r on r.IdPracownika = p.Id and r.Status = 1 and GETDATE() between r.Od and ISNULL(r.Do, '20990909')
	inner join hs on hs.root = r.IdPracownika
)
select CAST(hs.s + CAST(hs.hsid as VARBINARY(4)) as VARBINARY(8000)) sort
, p.Id
, REPLICATE('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', hs.h) + p.Nazwisko + ' ' + p.Imie from hs
left join Pracownicy p on p.Id = hs.hsid
where hs.root = 0
order by sort
option (maxrecursion 0)
    "></asp:SqlDataSource>
