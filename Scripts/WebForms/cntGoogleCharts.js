﻿var current_row = 0;
var chart = undefined;

function runChart(json) {
    google.charts.load('current', { packages: ["orgchart"] });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'ID');
        data.addColumn('string', 'Manager');
        data.addColumn('string', 'ToolTip');

        // Adding data to chart
        data.addRows(JSON.parse(json));

        // Create chart
        chart = new google.visualization.OrgChart(document.getElementById('mainOrgDiv'));

        // OnMouseOver
        // TODO
        // Debug jak naprawi tabele
        google.visualization.events.addListener(chart, 'onmouseover', function (row) {
            // getColalpsedNodes, i jeśli w tym będzie current_row to chart.collapse(..., false) a w przecwinym chart.collapse(..., true);s
            current_row = row.row;
            //chart.collapse(current_row, true);
        });

        // Prepare testDiv
        google.visualization.events.addListener(chart, 'ready', function () {
            $(".nodeClass").each(function (index, value) {
                var init_text = $(this).text();

                var infos = init_text.split(";");
                $(this).text("");       // cleanup

                // Szablon
                $(this).append("<div class='container container-node'>         \
                <div class='row'><div class='stanowisko'></div></div>                     \
                <div class='row'><div class='avatar collapse in'></div></div>                           \
                <div class='row'><div class='personalia collapse in'></div></div>                      \
                <div class='row'><div class='extend collapse'></div></div>                           \
                </div>                                      \
            ");

                var element = $(this).find('.container').find('.row');
                element.find('.stanowisko').append(infos[1]);
                element.find('.personalia').append("<div class='row'>" + infos[0] + "</div><div class='row main-desc'><b>Telefon kontaktowy:</b> " + infos[2] + "</div><div class='row main-desc'><b>E-mail:</b> " + infos[3] + "</div>");
                element.find('.extend').append("<div class='row'><b>Telefon kontaktowy:</b> " + infos[2] + "</div><div class='row'><b>E-mail:</b> " + infos[3] + "</div><div class='row'><b>Kompetencje:</b> " + (infos[5] == undefined ? 'brak' : infos[5]) + "</div>");
                element.find('.avatar').append("<img class='img-circle img-responsive img-node' src='http://cdn18.se.smcloud.net/t/photos/t/316145/otylosc-nadwaga-otyly-czlowiek-dieta_19800951.jpg' />");

                // Delete temp_span
                //temp_span.remove();
            });

            addCollapseFunctionToElements();
        });

        // Draw chart
        chart.draw(data, {
            allowHtml: true,
            allowCollapse: true,
            nodeClass: 'nodeClass',
            selectedNodeClass: 'noClass',
            colors: ['#fff']
        });
    }

    // Tooltips
    $(document).ready(function () {
        $("li").tooltip();
    });
}

function addCollapseFunctionToElements() {
    $('.container-node').click(function () {
        //$(this).find('.personalia').slideToggle("slow");
        $(this).find('.avatar').slideToggle("slow");
        $(this).find('.extend').slideToggle("slow");
    });
}