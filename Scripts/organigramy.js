﻿// widok samych struktur, 

function test() {
    $('*').each(function () {
        $(this).css("background", "rgb(" + Math.floor(Math.random() * 240) + ", " + Math.floor(Math.random() * 240) + ", " + Math.floor(Math.random() * 240) + ")");
        $(this).css("color", "rgb(" + Math.floor(Math.random() * 240) + ", " + Math.floor(Math.random() * 240) + ", " + Math.floor(Math.random() * 240) + ")");
        $(this).css("border-color", "rgb(" + Math.floor(Math.random() * 240) + ", " + Math.floor(Math.random() * 240) + ", " + Math.floor(Math.random() * 240) + ")");
        $(this).css("padding", Math.floor((Math.random() * 10)));
        $(this).css("font-size", Math.floor((Math.random() * 10) + 10));
    });
}

var current_row = 0;
var chart = undefined;
var fl = false;
var all_collapsed = true;

function ChangeFlag(state) {
    if (state == "horizontal") {
        fl = false;
    } else {
        fl = true;
    }
}

function runChart(json) {
    var list = JSON.parse(json);
    var out = $('<ul id="tree-data" style="display: none;">'),
	    idMap = {};
    var date = new Date();

    function get(id, text, parent_id) {
        var $el = idMap[id] || (idMap[id] = $('<li></li>'));
        if (text) {
            // Split string to array
            var array = text.split(";");

            var data = $el.children('div');
            if (!data.length) {
                data = $('<div class="data">').appendTo($el);
            }

            // Is Worker fired
            var fired = false;
            var worker_fire_date = new Date(array[10]);
            if (worker_fire_date.getTime() < date.getTime() && array[10] != "")
                fired = true;

            // arrar[5] // typ 0 - babka, 1 - facio, 2- struktura
            var image = (array[8].length > 0 ? "Upload/Images/" + array[8] : (parseInt(array[5]) == 1 ? 'Content/images/male.png' : 'Content/images/female.png'));

            data.html("<div class='main-worker-info "+(fired == true ? 'fired' : '')+"'><div class='row cols-row'><div class='col-left col-lg-12'>\
                <div class='worker-id' id='worker_" + id + "'>" + id + "</div><div class='worker-parent'>" + parent_id + "</div><div class='row worker-title'>" + array[1] + "</div> \
                <div class='row worker-avatar'><img src='"+image+"' class='img-responsive img-circle' /></div>                     \
                <div class='row worker-name'>" + array[0] + "</div> \
                <div class='row worker-phone'><span class='glyphicon glyphicon-phone non-jorg-span' aria-hidden='true'></span>" + array[2] + "</div> \
                <div class='row worker-mail'><span class='glyphicon glyphicon-envelope non-jorg-span' aria-hidden='true'></span><a class='mailhref' href='mailto:" + array[3] + "'>" + array[3] + "</a></div> \
                <div class='row worker-counter'>0/0</div>\
                \
                </div>\
                <div class='col-right col-lg-6 collapse vcenter'><p><b>Opis:</b> "+array[7]+"</p><p><b>Zatrudniony od</b>:<br>"+array[9].substr(0,array[9].indexOf(' '))+"</p><p><b>Zatrudniony do:</b><br>"+(array[10] != "" ? array[10].substr(0,array[10].indexOf(' ')) : 'Bezterminowo')+"</p></div>\
                </div></div> ");

            // Czy jest strukturą
            if (array[5] == 2) {
                $el.html("<div class='main-worker-info struktura'><div class='worker-id' id='worker_" + id + "'>" + id + "</div><div class='worker-parent'>" + parent_id + "</div><div class='row'>" + array[0] + "</div><div class='row worker-counter'>0/0</div></div>");
            }
        }
        return $el;
    }

    function addChild($parent, $child) {
        var $list = $parent.children('ul');
        if (!$list.length) {
            if ($parent.parents("ul").length > 2 && fl == true) {
                $list = $('<ul type="vertical">').appendTo($parent);
            } else {
                $list = $('<ul type="horizontal">').appendTo($parent);
            }
        }
        $list.append($child);
    }

    var iter = 0;

    while (list.length > 0 && list.length != iter) {


        var item = list[iter];
        var $el = get(item[0], item[1], item[2]);

        if (item[2]) {
            if (!idMap[item[2]]) {
                iter++;
                continue;
            }

            addChild(get(item[2]), $el);
        } else {
            out.append($el);
        }

        list.splice(iter, 1);
        iter = iter % list.length;
    }

    out.find("ul").each(function (i) {
        $(this).find("li:last-child").addClass("last");
    });

    $('#result').append(out);

    // Listing 
    $("#tree-data").find('li').each(function (index) {
        var id = $(this).find('.worker-id').first();
        id.parent().find('.worker-counter').text(GetDirectChilds(parseInt(id.text())) + "/" + GetAllChilds($(this).find('li')));
    });
    $(".worker-counter").each(function (index) {
        if ($(this).text() == "0/0") {
            $(this).hide();
        }
    });
    // End listing

    $("#tree-data").jOrgChart({
        chartElement: $("#tree-view")
    });
}

function GetAllChilds(array) {
    var num = 0;
    if (array.length > 0) {
        array.each(function () {
            if (!$(this).children().hasClass("struktura")) {
                num += 1;
            }
        });
    }

    return num;
}

function GetDirectChilds(id) {
    var num = 0;
    $('.worker-parent').each(function (index) {
        if (!$(this).parent().hasClass("struktura")) {
            if ($(this).text() == id) {
                num += 1;
            }
        }
    });
    return num;
}

function ChangeAllCollapse() {
    $(".node, .multi-tree .content").each(function () {
        CollapseElementOfTree($(this));
    });

    // Change variable state
    all_collapsed = !all_collapsed;

    // Change button state
    if (all_collapsed)
        $(".collapse-expand").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
    else
        $(".collapse-expand").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");

    return false;
}

// Branch: dev#11
function CollapseElementOfTree(element) {
    if (element.find('.struktura').length > 0) {
        // Prevent structures collapse
        return false;
    }

    var test = element.parent().parent().parent().parent();
    if (!element.find('.col-right').is(":visible")) {
        element.find('.col-left').removeClass('col-lg-12').addClass('col-lg-6');
        element.find('.cols-row, .main-worker-info').css("width", "300px");
        if (element.parents().find('.multi-tree').length > 0) {
            element.css("width", "300px");
            element.css("z-index", "5000000");
        }
        setTimeout(function () { element.find('.col-right').toggle(); }, 300);
    } else {
        setTimeout(function () {
            element.find('.col-left').removeClass('col-lg-6').addClass('col-lg-12');
            element.find('.cols-row, .main-worker-info').css("width", "150px");
            if (element.parents().find('.multi-tree').length > 0) {
                element.css("width", "150px");
                element.css("z-index", "0");
            }
        }, 100);
        element.find('.col-right').toggle();
    }
}
// /Branch: dev#11

// Branch: dev#6
function ExportToPDF() {
    html2canvas($("#tree-view"), { type: 'view' }).then(function (canvas) {
        var img = canvas.toDataURL("image/png");
        $('#img').append('<style>body { font-family: tahoma; } </style>');

        setTimeout(function () {
            var doc = new jsPDF('l', 'mm', [297, 210]);
            doc.addImage(img, 'PNG', 0, 0, 300, 200);
            doc.save('sample-file.pdf');
        }, 1000);
    });
}
// /Branch: dev#6

// Branch: dev#6
function search() {
    var search_pattern = $("#searchBox").val().toLowerCase();
    $("#searchBox").val("");
    
    $(".node, .multi-tree .content").removeClass("selected");
    if (search_pattern.length > 0) {
        var first = true;
        $(".node, .multi-tree .content").each(function (index) {

            var string = $(this).text().toLowerCase();
            if (string.search(search_pattern) != -1) {
                //$(this).css("color", "red");
                $(this).addClass("selected");
                if (first) {
                    $('html, body').animate({
                        scrollTop: $(this).offset().top
                    }, 2000);
                    first = false;
                }
            }
        });
    }

    return false;
}
// /Branch: dev#6

// Branch: dev#11
$('#menu').keydown(function (event) {
    var keyCode = (event.keyCode ? event.keyCode : event.which);
    if (keyCode == 13) {
        search();
        event.preventDefault();
    }

});
// /Branch: dev#11

function pageLoad() {
    $('body').on('click', 'input[type=checkbox]', function () {
        var $attr = $(this).attr('data-element');
        if (!$(this).is(":checked")) {
            $("." + $attr).css("display", "none");
            $("input[data-element=" + $attr + "]").attr("checked", false);
        } else {
            $("." + $attr).css("display", "");
            $("input[data-element=" + $attr + "]").attr("checked", true);
        }
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $('#filters_Click').popover({
        html: true,
        container: '#menu',
        title: function () {
            return $("#filters-pop").find('.head').html();
        },
        content: function () {
            return $("#filters-pop").find('.content').html();
        },
        trigger: "click",
        placement: "bottom"
    });

    $(function () {
        console.log("node click init");
        $(".node, .multi-tree .content").click(function () {
            CollapseElementOfTree($(this));
            console.log('test');
        });
    });

    $(function () {
        //https://bootstrap-datepicker.readthedocs.io/en/latest/options.html 
        $(".datepicker").datepicker({
            format: "yyyy-mm-dd",
            startView: 2,
            daysOfWeekDisabled: "0",
            language: "pl",
            calendarWeeks: true
        });

        $(".datepickero").on("click", function () {
            $(this).parent().find(".datepicker").datepicker();
        });
    });
}